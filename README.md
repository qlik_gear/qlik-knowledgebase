# Qlik Knowledge Base 

## Run it

```
    docker-compose up
```

## Access

- Website: http://localhost

- Database/Phpmyadmin: http://localhost:8080

```
Username: qlik
Password: Qlik1234
```

- Container Logs (Dozzle): http://localhost:8888

- REST-API Docs: https://gitlab.com/qlik_gear/question2answer-rest-api.git









